﻿# Check my Menu - Web app to estimate your meal's glucydic index.
**French version of README.**

## Version & Licence

**version 1.0**
**Licence CC-BY-NC-ND**

*dernière mise à jour de l'application :* **9/05/19**
*dernière mise à jour de la notice :* **9/05/19**

*Créée par* **Thomas CADEI**
*Pour toute information ou relevé d'un dysfonctionnement :*
**t.cadei@live.fr**


## À propos :

Cette application permet l'inscription et la connexion/déconnexion des utilisateurs. Une fois connectés, ils peuvent estimer l'indice glycémique total de leur repas. Des suggestions d'aliments défilent en diaporama sur l'écran d'accueil, il suffit de cliquer sur celles-ci pour composer le menu. Un message flash indique si l'équilibre du repas est atteint ou non. Il leur est également proposé d'appeler un spécialiste directement depuis l'application.


## Capture :

![Logo getCodingKnowledge](ressources/capture-glycindex.png)


## Pré-requis :

* un smartphone ou tablette ou ordinateur fonctionnel avec un système d'exploitation (Windows, Linux ou MacOS), relié à un écran et disposant d'une souris ou d'un pad tactile. 1 
* une connexion internet avec un débit suffisant & un navigateur moderne (Google Chrome, Mozilla Firefox, Opera, ...). 2


## Installation :

Utiliser l'application en ligne ou bien télécharger l'ensemble du répertoire et le copier sur votre serveur avant de lancer le fichier **index.php**.


## Exemple d'utilisation :

Lancer l'application. 
Pour une première visite, cliquer sur le lien pour s'inscrire et remplir le formulaire. 
Se connecter.
(compte utilisateur de démonstration : id="thomas" mdp="simplon")
Cliquer sur un ou plusieurs aliments défilant dans le diaporama afin de constituer un menu (possibilité de SWIPE sur écran tactile).
Les aliments sélectionnés sont listés au centre de la page.
Il est possible de retirer des aliments du menu en cliquant sur l'icône en forme de croix à droite de chaque aliment de la liste.
En pied de page, le total de l'indice glycémique est indiqué, associé à un commentaire ainsi qu'à un bouton de prise de RDV par téléphone.

## WEBMASTERS : ajout d'aliments :

Les données concernant les aliments proposés dans le diaporama sont extraites d'un fichier .json situé dans le dossier éponyme.
Pour ajouter des aliments à la suite du fichier, respecter précisément le format .json, incrémenter la valeur de la propriété "id" (inetger), entrer la valeur de la propriété "product" (string) qui est le nom de l'aliment, ainsi que celle de la propriété "index" (integer) qui est l'indice glycémique pour 100g de cet aliment.
Dans le dossier "images", ajouter la miniature correspondant à chaque nouveau produit et la renommer "**id**.jpeg" où **id** est la valeur de la propriété "id" du produit.
Pour optimiser l'expérience utilisateur, il serait opportun d'ajouter une ligne de code au fichier "index.php", dans la liste correspondant à l'id "indic-list" et en respectant le format, de sorte que le diaporama affiche un nombre d'indicateurs égal au nombre de produits.


## Diagramme Use Case :

![Logo getCodingKnowledge](ressources/usercase-checkmymenu.png)

## Wireframe :

![Logo getCodingKnowledge](ressources/wireframe-checkmymenu.png)




