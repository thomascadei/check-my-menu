<?php
require("connection.php");
if (!empty($_POST)) {
    if (empty($_POST['username'])) {
        $message = '<p class="text-center fail">Please enter a username.</p>';
    }

    if (empty($_POST['password'])) {
        $message = '<p class="text-center fail">Please enter a password.</p>';
    }

    if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
        $message = '<p class="text-center fail">Invalid E-Mail Address.</p>';
    }
    $query = "
        SELECT
            1
        FROM users
        WHERE
            username = :username
    ";
    $query_params = array(
        ':username' => trim(htmlspecialchars($_POST['username']))
    );
    try {
        $stmt = $db->prepare($query);
        $result = $stmt->execute($query_params);
    } catch (PDOException $ex) {
        echo "
        <script type='text/javascript'>
        alert('Account creation failed !');
        window.location.href='register.php';
        </script>";    }
    $row = $stmt->fetch();
    if ($row) {
        $message = '<p class="text-center fail">Username already in use.</p>';
    }
    $query = "
        SELECT
            1
        FROM users
        WHERE
            email = :email
    ";
    $query_params = array(
        ':email' => trim(htmlspecialchars($_POST['email']))
    );
    try {
        $stmt = $db->prepare($query);
        $result = $stmt->execute($query_params);
    } catch (PDOException $ex) {
        echo "
        <script type='text/javascript'>
        alert('Account creation failed !');
        window.location.href='register.php';
        </script>";    }
    $row = $stmt->fetch();
    if ($row) {
        $message = '<p class="text-center fail">Email adress already registered.</p>';
    }
    $query = "
        INSERT INTO users (
            username,
            password,
            salt,
            email
        ) VALUES (
            :username,
            :password,
            :salt,
            :email
        )
    ";
    $salt = dechex(mt_rand(0, 2147483647)) . dechex(mt_rand(0, 2147483647));
    $password = hash('sha256', trim(htmlspecialchars($_POST['password'])) . $salt);

    for ($round = 0; $round < 65536; $round++) {
        $password = hash('sha256', $password . $salt);
    }
    $query_params = array(
        ':username' => trim(htmlspecialchars($_POST['username'])),
        ':password' => $password,
        ':salt' => $salt,
        ':email' => trim(htmlspecialchars($_POST['email']))
    );
    try {
        $stmt = $db->prepare($query);
        $result = $stmt->execute($query_params);
    } catch (PDOException $ex) {
        echo "
        <script type='text/javascript'>
        alert('Account creation failed !');
        window.location.href='register.php';
        </script>";
    }
    echo "
        <script type='text/javascript'>
        alert('Account created ! Please log in.');
        window.location.href='register.php';
        </script>";
    die("Redirecting to register.php");
}
?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Check My Menu | Register</title>
    <link rel="shortcut icon" href="ressources/favicon.png" type="image/png">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Dosis|Great+Vibes" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">">
</head>

<body>
    <?php
    include("header0.php");
    ?>
    <main class="container-fluid">
        <div class="row justify-content-center my-2">
            <h2 class="marg">Please sign in:</h2>
        </div>
        <div class="row justify-content-center">
            <div class="col-12">
                <?php
                if (isset($message)) {
                    echo $message;
                }
                ?>
            </div>
        </div>
        <form action="register.php" method="post" class="row justify-content-center">
            <div class="col-8 form-group row justify-content-center">
                <label class="col-12" for="regname">Username:</label>
                <input class="col-12 mb-3" type="text" name="username" value="" id="regname" required>
                <label class="col-12" for="regmail">E-mail:</label>
                <input class="col-12 mb-3" type="text" name="email" value="" id="regmail" required>
                <label class="col-12" for="regpass">Password:</label>
                <input class="col-12 mb-3" type="password" name="password" value="" id="regpass" required>
                <input type="submit" value="Register" class="btn btn-primary btn-lg m-auto">
                <input type="reset" value="Reset" class="btn btn-primary btn-lg m-auto">
            </div>
        </form>
        <p class="mt-3 text-center">
            Already registered ?
            <a href="login.php" class="text-primary">Please log in</a>
        </p>
    </main>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>