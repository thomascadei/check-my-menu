<?php
require("connection.php");
// reCAPTCHA
if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['recaptcha_response'])) {
    $recaptcha_url = 'https://www.google.com/recaptcha/api/siteverify';
    $recaptcha_secret = '6LfJpKIUAAAAADGltm-rKegCn-K5LElvKKxkytHq';
    $recaptcha_response = $_POST['recaptcha_response'];
    $recaptcha = file_get_contents($recaptcha_url . '?secret=' . $recaptcha_secret . '&response=' . $recaptcha_response);
    $recaptcha = json_decode($recaptcha);
    if ($recaptcha->score >= 0.5) {
        //reCAPTCHA OK
        $submitted_username = '';
        if (!empty($_POST)) {
            $query = "
            SELECT
                id,
                username,
                password,
                salt,
                email
            FROM users
            WHERE
                username = :username";
            $query_params = array(
                ':username' => trim(htmlspecialchars($_POST['username']))
            );
            try {
                $stmt = $db->prepare($query);
                $result = $stmt->execute($query_params);
            } catch (PDOException $ex) {
                $message = '<p class="text-center fail">Echec. ' + $ex->getMessage();
                +'</p>';
            }
            $login_ok = false;
            $row = $stmt->fetch();
            if ($row) {
                $check_password = hash('sha256', trim(htmlspecialchars($_POST['password'])) . $row['salt']);
                for ($round = 0; $round < 65536; $round++) {
                    $check_password = hash('sha256', $check_password . $row['salt']);
                }
                if ($check_password === $row['password']) {
                    $login_ok = true;
                } else {
                    $message = '<p class="text-center fail">Password authentication failed.</p>';
                }
            } else {
                $message = '<p class="text-center fail">Login failed.</p>';
            }
            if ($login_ok) {
                unset($row['salt']);
                unset($row['password']);
                $_SESSION['user'] = $row;
                header("Location: index.php");
                die("Redirecting to: index.php");
            } else {
                $message = '<p class="text-center fail">Login failed.</p>';
                $submitted_username = trim(htmlspecialchars($_POST['username'], ENT_QUOTES, 'UTF-8'));
            }
        } else {
            $message = '<p class="text-center fail">Data is missing.</p>';
        }
    } else {
        $message = '<p class="text-center">Please fill the form.</p>';
    }
}
?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Check My Menu | Login</title>
    <link rel="shortcut icon" href="ressources/favicon.png" type="image/png">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Dosis|Great+Vibes" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <?php
    include("header0.php");
    ?>
    <main class="container-fluid">
        <div class="row justify-content-center mt-5">
            <h2 class="marg">Please log in :</h2>
        </div>
        <div class="row justify-content-center">
            <div class="col-12">
                <?php
                if (isset($message)) {
                    echo $message;
                }
                ?>
            </div>
        </div>
        <form action="login.php" method="post" class="row justify-content-center">
            <div class="col-8 form-group row justify-content-center">
                <label class="col-12" for="logname">Username:</label>
                <input class="col-12 mb-3" type="text" name="username" id="logname" value="<?php echo $submitted_username; ?>" />
                <label class="col-12" for="logpass">Password:</label>
                <input class="col-12 mb-3" type="password" name="password" id="logpass" value="">
                <input type="submit" value="Login" class="btn btn-primary btn-lg m-auto">
                <input type="reset" value="Reset" class="btn btn-primary btn-lg m-auto">
                <input type="hidden" name="recaptcha_response" id="recaptchaResponse">
            </div>
        </form>
        <p class="mt-3 text-center">
            First visit ?
            <a href="register.php" class="text-primary">Please sign in</a>
        </p>
    </main>
    <script src="https://www.google.com/recaptcha/api.js?render=6LfJpKIUAAAAAAy6DYieVi8J9nEsLx10hf5NNPyg"></script>
    <script>
        grecaptcha.ready(function() {
            grecaptcha.execute('6LfJpKIUAAAAAAy6DYieVi8J9nEsLx10hf5NNPyg', {
                action: 'login'
            }).then(function(token) {
                var recaptchaResponse = document.getElementById('recaptchaResponse');
                recaptchaResponse.value = token;
            });
        });
    </script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>