class Data {
	constructor(thejson) {
		this.thejson = thejson;
		this.ohmydata = this.ajaxQuery();
	}

	ajaxQuery() {
		// Appel Ajax 
		this.ajaxGet(this.thejson, (reponse) => {
			let datarray = JSON.parse(reponse);
			var filter = document.getElementById('filter');
			var result = document.getElementById('result');
			var show = document.getElementById('show');
			var flash = document.getElementById('flash');
			var footcall = document.getElementById('footcall');
			var total = 0;
			var callbtn = document.createElement("a");
			callbtn.innerHTML = "Call a specialist";
			var callclass = document.createAttribute("class");
			callclass.value = "btn btn-dark text-light";
			callbtn.setAttributeNode(callclass);
			var calltel = document.createAttribute("href");
			calltel.value = "tel:+33632096792";
			callbtn.setAttributeNode(calltel);
			footcall.appendChild(callbtn);
			callbtn.style.visibility = "hidden";
			datarray.forEach(function (element) {
				var id = element.id;
				var product = element.product;
				var index = element.index;
				var button = document.createElement("div");
				var mini = document.createElement("img");
				var title = document.createElement("p");
				title.innerHTML = product;
				mini.src = "images/"+id+".jpeg";
				button.appendChild(mini);
				button.append(title);
				var btnclass = document.createAttribute("class");
				btnclass.value = "btn btn-secondary carousel-item mb-2";
				button.setAttributeNode(btnclass);
				filter.appendChild(button);
				var add = parseInt(index);

				button.addEventListener('click', function () {
					var line = document.createElement("p");
					line.innerHTML = product + ' ';
					result.appendChild(line);
					var mark = document.createElement("hr");
					result.appendChild(mark);
					total += add;
					show.innerHTML = total;
					if (total < 60) {
						flash.innerHTML = "Not even enough for starving !"
						var sliclass = document.createAttribute("class");
						sliclass.value = "bg-danger text-center my-1";
						flash.setAttributeNode(sliclass);
						show.style.color = '#c71c22';
						callbtn.style.visibility = "visible";
						show.style.padding = '5px';
					}
					else if (total > 60 && total <= 120) {
						flash.innerHTML = "You eat like a bird my friend !"
						var sliclass = document.createAttribute("class");
						sliclass.value = "bg-warning text-center my-1";
						flash.setAttributeNode(sliclass);
						show.style.color = '#dd5600';
						callbtn.style.visibility = "visible";
						show.style.padding = '5px';
					}
					else if (total > 120 && total < 200) {
						flash.innerHTML = "Nice, your menu seems balanced !"
						var balclass = document.createAttribute("class");
						balclass.value = "bg-success text-center my-1";
						flash.setAttributeNode(balclass);
						show.style.color = '#73a839';
						callbtn.style.visibility = "visible";
						show.style.padding = '5px';
					}
					else if (total > 200 && total < 250) {
						flash.innerHTML = "You're hungry as a wolf !"
						var fatclass = document.createAttribute("class");
						fatclass.value = "bg-warning text-center my-1";
						flash.setAttributeNode(fatclass);
						show.style.color = '#dd5600';
						callbtn.style.visibility = "visible";
						show.style.padding = '5px';
					}
					else if (total > 250) {
						flash.innerHTML = "Folk, you're gonna explode !"
						var fatclass = document.createAttribute("class");
						fatclass.value = "bg-danger text-center my-1";
						flash.setAttributeNode(fatclass);
						show.style.color = '#c71c22';
						callbtn.style.visibility = "visible";
						show.style.padding = '5px';
					}
					else {
						flash.innerHTML = "How did you do that ?";
						callbtn.style.visibility = "visible";
						show.style.padding = '5px';
					}
					var close = document.createElement("i");
					var closeclass = document.createAttribute("class");
					closeclass.value = "fa fa-window-close float-right";
					close.setAttributeNode(closeclass);
					line.appendChild(close);
					close.addEventListener('click', function () {
						line.parentNode.removeChild(line);
						mark.parentNode.removeChild(mark);
						total -= add;
						show.innerHTML = total;
						if (total <= 0) {
							flash.innerHTML = "Not even enough for starving !"
							var sliclass = document.createAttribute("class");
							sliclass.value = "bg-danger text-center my-1";
							flash.setAttributeNode(sliclass);
							show.style.color = '#c71c22';
							callbtn.style.visibility = "hidden";
						}
						else if (total > 0 && total < 60) {
							flash.innerHTML = "Not even enough for starving !"
							var sliclass = document.createAttribute("class");
							sliclass.value = "bg-danger text-center my-1";
							flash.setAttributeNode(sliclass);
							show.style.color = '#c71c22';
						}
						else if (total > 60 && total <= 120) {
							flash.innerHTML = "You eat like a bird my friend !"
							var sliclass = document.createAttribute("class");
							sliclass.value = "bg-warning text-center my-1";
							flash.setAttributeNode(sliclass);
							show.style.color = '#dd5600';
						}
						else if (total > 120 && total < 200) {
							flash.innerHTML = "Nice, your menu seems balanced !"
							var balclass = document.createAttribute("class");
							balclass.value = "bg-success text-center my-1";
							flash.setAttributeNode(balclass);
							show.style.color = '#73a839';
						}
						else if (total > 200 && total < 250) {
							flash.innerHTML = "You're hungry as a wolf !"
							var fatclass = document.createAttribute("class");
							fatclass.value = "bg-warning text-center my-1";
							flash.setAttributeNode(fatclass);
							show.style.color = '#dd5600';
						}
						else if (total > 250) {
							flash.innerHTML = "Folk, you're gonna explode !"
							var fatclass = document.createAttribute("class");
							fatclass.value = "bg-danger text-center my-1";
							flash.setAttributeNode(fatclass);
							show.style.color = '#c71c22';
						}
						else {
							flash.innerHTML = "How did you do that !?"
							var fatclass = document.createAttribute("class");
							fatclass.value = "bg-danger text-center my-1";
							flash.setAttributeNode(fatclass);
							show.style.color = '#c71c22';
						}
					})
				});
			});
		});
	}

	getAjaxQuery() {
		return this.ohmydata;
	}

	ajaxGet(url, callback) {
		var req = new XMLHttpRequest();
		req.open("GET", url);
		req.addEventListener("load", function () {
			if (req.status >= 200 && req.status < 400) {
				// Appelle la fonction callback en lui passant la réponse de la requête
				var reponse = req.responseText
				callback(reponse);
			}
			else {
				console.error(req.status + " " + req.statusText + " " + url);
			}
		});
		req.addEventListener("error", function () {
			console.error("Erreur réseau avec l'URL " + url);
		});
		req.send(null);

	}
}