<header class="fixed-top">
  <nav class="navbar navbar-expand-lg navbar-dark bg-primary w-100">
    <img src="images/logo.svg" class="img-fluid" id="logo">
    <div class="navbar-brand">
      <h1 class="col-12 pt-2">Check my Menu</h1>
      <span class="col-12" id="punch">Your meal's glucydic index !</span>
    </div>
  </nav>
</header>