<?php
require("connection.php");
if (empty($_SESSION['user'])) {
  header("Location: login.php");
  die("Redirecting to login.php");
}
// Everything below this point in the file is secured by the login system
?>
<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Check My Menu | Home</title>
  <link rel="shortcut icon" href="ressources/favicon.png" type="image/png">
  <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css?family=Dosis|Great+Vibes" rel="stylesheet">
  <link rel="stylesheet" href="css/style.css">
</head>
<body class="container-fluid">
  <?php
  include("header.php");
  ?>
  <main>
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators" id="indic-list">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
    <li data-target="#myCarousel" data-slide-to="3"></li>
    <li data-target="#myCarousel" data-slide-to="4"></li>
    <li data-target="#myCarousel" data-slide-to="5"></li>
    <li data-target="#myCarousel" data-slide-to="6"></li>
    <li data-target="#myCarousel" data-slide-to="7"></li>
    <li data-target="#myCarousel" data-slide-to="8"></li>
    <li data-target="#myCarousel" data-slide-to="9"></li>
    <li data-target="#myCarousel" data-slide-to="10"></li>
    <li data-target="#myCarousel" data-slide-to="11"></li>
  </ol>
      <div id="filter" class="carousel-inner">
        <div class="btn btn-secondary carousel-item mb-2 active"><img src="images/start.png">
          <p>Clic on food to select</p>
        </div>
      </div>
      <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
    <section class="row justify-content-center align-items-end" id="menu">
      <div id="result" class="mt-2 col-12 col-md-6">
        <hr>
      </div>
    </section>
    <footer class="bg-primary fixed-bottom text-center">
      <div id="flash"></div>
      Your menu's G.I. is :
      <span id="show"></span>
      <div id="footcall" class="m-2"></div>
    </footer>
  </main>
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src=”js/jquery.mobile.custom.min.js”></script>  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <script type="application/javascript" src="js/data.js"></script>
  <script type="application/javascript" src="js/app.js"></script>
</body>
</html>