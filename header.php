<header class="fixed-top">
  <nav class="navbar navbar-expand-lg navbar-dark bg-primary w-100">
    <img src="images/logo.svg" class="img-fluid" id="logo">
    <div class="navbar-brand">
      <h1 class="col-12 pt-2">Check my Menu</h1>
      <span class="col-12" id="punch">Your meal's glucydic index !</span>
    </div>
    <div class="row justify-content-end align-items-end">
      <h3>
        Welcome <span class="text-capitalize"><?php echo htmlentities($_SESSION['user']['username'], ENT_QUOTES, 'UTF-8'); ?></span>, choose your menu !
      </h3>
      <a href="logout.php" class="ml-5" title="Log out"><i class="fa fa-sign-out fa-2x text-dark"></i></a>

    </div>
  </nav>
</header>